boxRouter.post('/upload', multer.single('file'), async (req: any, res: Response) => { // multer.single('file'),
    
    try{
        console.log("req.file", req.file)

        console.log("binary", req.file.buffer)
        var option :any = {headers: {
            "Content-Length" : req.file.size,
            "Content-Type": req.file.mimetype
         } };
        var option :any = {headers: {}}
        
        
        // option.headers['Authorization'] = "Bearer ya29.a0ARrdaM9mEKOOkO6s1hXbHxBaCTryW9UIhvG_YF2FRwqJLb0Cymmwqt_LRWA23bkAzG4BMNG2n8hydoZk1ApzfiNeNLRBVfcTVv9LtIOIfzggE3RI78ydp2DZ672V2VliE2bJn0ttFMY_UBJznyWz9ES4Pxvb"
        // var data = Buffer.from(req.file.buffer)
        var data = Buffer.from(req.file.buffer).toString('base64');
        var buf = Buffer.from(data, 'base64');

        console.log("binary", data)
        var result = {};
        // var url = "https://storage.googleapis.com/upload/storage/v1/b/sms-stage.appspot.com/o?uploadType=media&name=test1";
        option.headers['boxConfigToken'] = "U2FsdGVkX1/N/zDrxOQwyRo2hoNydmjuPq87nB2kVXFaJ3XJ8IeDf8JlhyD179lWirwZ0UN/X19bTOnv9rZ23+GGcw631EqYcicaJN4xPFtFTRsYmCJTofaWerAbp0zlYsYP7fSTdoldNJyg6P10l20bEyXXjduDQ8T1RAgKCoNNd3kgGhl5naQDAsJt8aqpjZTdNOKTdD1DVwE8i/Ou7KtbGbYWNksf1Z8AjLRM/vFwOQhcH0cKf4ySI0eByLSXATAB+o0zb9zxuQ4eIYDSoil8N/tICPVKQ8zL+nkigmzg6IAp+NEHyWc0ikAJbRbLbR+i1Xzq5nm/fRzTlKBrqoqSsuq8OxO+g9AKBmQ97p7sFcBINbOUO6FZe9XV3tFmTJ+oPN7xLbQq1qUY/szcHOtSeKfk2uRRiH77SIHIW5nd9ErxCoDiciVcW+tUYtqO0UFHqFGW3UVnmcJXwAs+dA==";
        var url = "http://localhost:8080/api/box/googlecloudstorage/uploadFile"
        
        //  req.file.buffer = data
        req.file.filename = req.file.originalname;
        var payload = {
            "parameters" :{
                bucket: "sms-stage.appspot.com",
                "fileName": "folder1/test3",
                attachments: [req.file]
            }
        }

        // console.log("payload", payload)
        var response:any = await axios.post(url, payload, option)
        console.log("response", response)
        res.status(200).json({data: response.data});
        // res.status(200)
    } catch(err){
        console.log("error: ", err)
        res.status(400).json({error: err});
    }    
});