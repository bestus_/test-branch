 if (this.stream != null) {
        console.log("Stream: ", this.stream)
        if(this.capturingFeature == 'video' && (this.webCamStream || this.screenStream)){
          this.recorder = new MediaRecorder(this.stream, { mimeType: "video/webm" });
        } else {
          this.recorder = new MediaRecorder(this.stream);
        }
        this.recorder.onstart = () =>
          this.ngZone.run(() => {
            console.log("MediaRecorder onstart");
            this.isRecording$.next(true);
            // this.cd.detectChanges();
            console.log("isRecording became true");
          });

        // this.recorder.setMaxFileSize(5000000); 

        this.recorder.ondataavailable = (event) =>
          this.ngZone.run(async () => {
            console.log("event-------------------->",this.firstBlob.length, event.data)
            this.recorder.stop();
            this.recorder.start(30000);
            this.data =  [event.data] //[...this.data, event.data]; //
            var fileText:any = await blobToBase64(event.data);
            console.log("test test test:   ", fileText)
            console.log("test test test:   length: ", fileText.length)
            var sendData = "";
            fileText = fileText.substr(46, fileText.length);
            
            if(this.firstBlob.length == 0){
              this.firstBlob = [event.data];
              // this.data = [...this.data, event.data];
              console.log("before 111111", fileText)
              sendData = fileText;
              this.prefixtext  = fileText.substr(0, 5180);
              
              // fileText = fileText.substr(0, 46);
              console.log("this.prefixtext", this.prefixtext)
            } else {
              // fileText = fileText.substr(380, fileText.length);
              console.log("222222 fileText:", fileText)
              console.log("this.prefixtext length", this.prefixtext.length)
              console.log("this.prefixtext", this.prefixtext)
              sendData = this.prefixtext + fileText;
              console.log("222222", sendData)
              console.log("sendData length", sendData.length)
            }
            
            //else {
            //   console.log("222222", this.firstBlob.length)
            //   this.data = [];
            //   this.data = [...this.firstBlob, event.data]
            // }
            if (this.data.length > 0) {
              // if(this.capturingFeature == 'video'){
              //   if(this.webCamStream || this.screenStream){
                console.log("on data available", this.data)
                  await this.fileToBeUploaded(sendData);
              //   } else if(!this.webCamStream && !this.screenStream && this.micStraem){
              //     await this.uploadFileToDrive(this.data, { 'name': `audio${new Date().getTime()}.mp3`, 'mimeType': "audio/webm", 'parents': [this.UploadFolderId] });
              //   }
              // } 
              // if(this.capturingFeature == 'screenshot' && this.micStraem) {
              //   await this.uploadFileToDrive(this.data, { 'name': `audio${new Date().getTime()}.mp3`, 'mimeType': "audio/webm", 'parents': [this.UploadFolderId] });
              // }
            }
            // this.data = [];
          });

        this.recorder.onstop = event =>
          this.ngZone.run(async () => {
            console.log('inside Recorder.Onstop');
            if (this.screenStream) {
              this.screenStream.getTracks().forEach(t => t.stop());
            }
            if (this.webCamStream) {
              this.webCamStream.getTracks().forEach(t => t.stop());
            }
            if(this.micStraem){
              console.log("This is here for test of stopping microphone--------------------------------------****************************************************************")
              this.micStraem.getTracks().forEach(t => t.stop());
            }
            await this.fileToBeUploaded(this.data);
          });
        // this.recorder.start(30000); //120000 //
        // this.intervalIDStartStop = setInterval(() => {
        //   this.recorder.stop();
        //   this.recorder.start();
        // }, 30000)
        // this.intervalIDStartStop = setInterval(() => {
      }











      
function blobToBase64(blob) {
    return new Promise((resolve, _) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result);
      reader.readAsDataURL(blob);
    });
  }
  
  const b64toBlob = (b64Data, contentType='', sliceSize=512) => {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
  
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
  
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
  
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
  
    const blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }