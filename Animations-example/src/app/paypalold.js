  async function insertAndUpdateSubscription(transaction_obj, planData) {

    var plan_type = planData.plan_type; // Pro, Premium, Business etc
    var product = planData.product;
    var frequency = planData.frequency;

    console.log("record is going to update for subscription collection");
    const query_for_subscription = datastoreClient.createQuery("subscription")
      .filter("email", "=", transaction_obj.license_email)
      .filter("product", "=", product);
    var result = null;
    let existingSub = await databaseManager.getKindData(query_for_subscription);

    if(existingSub) {
        existingSub.status = 'Active';
        existingSub.paypal_subscription_id = transaction_obj.subscription_id;
        existingSub.frequency = (frequency) ? frequency : entity.frequency; // TODO - check if defaulting with entity.frequency required
        existingSub.email = transaction_obj.license_email;
        var current_date = new Date();
        let valid_upto = new Date(existingSub.valid_upto);
        existingSub.last_renewal_at = new Date();
        console.log("current date:", current_date);
        console.log("valid upto date", valid_upto);

        //if plan has balance type then add balances array to subscriptions
        if(planData.balances && planData.balances.length > 0 && planData.provideBalance){
          if(!existingSub.balances) existingSub.balances = [];
          var planBalances = planData.balances;
          existingSub.balances = payment.insertBalanceToSubscription(existingSub.balances, planBalances, plan_type)
        }

        // if (current_date > valid_upto) {
      
        //checking the frequency of the plan
        //set validity from current date
        if (frequency == 'MONTH') {
          existingSub.valid_upto = new Date(new Date(current_date).setMonth(current_date.getMonth() + 1));
        } else {
          existingSub.valid_upto = new Date(new Date(current_date).setMonth(current_date.getMonth() + 12));
        }
        // } else {
        //     if (frequency == 'MONTH') {
        //         existingSub.valid_upto = new Date(new Date(valid_upto).setMonth(valid_upto.getMonth() + 1));
        //     } else {
        //         existingSub.valid_upto = new Date(new Date(valid_upto).setMonth(valid_upto.getMonth() + 12));
        //     }
        // }

        existingSub.valid_upto = setEndTimeToDate(existingSub.valid_upto);
      
        if (transaction_obj.comments) {
            existingSub.comment = transaction_obj.comments;
        }
      
        if (transaction_obj.payer_email) {
            existingSub.payer_email = transaction_obj.payer_email
        }
      
        existingSub.product = product;
        var existingPlan = existingSub.plan_type;
        existingSub.plan_type = plan_type;

        result = await databaseManager.updateKind(existingSub);
        result.existingPlan = existingPlan;
        await payment.updateProductUser(existingSub.email, existingSub.product);
    } else {
        var subscription_obj = {};
        subscription_obj.email = transaction_obj.license_email;
        subscription_obj.paypal_subscription_id = transaction_obj.subscription_id;
        subscription_obj.started_at = new Date();
        subscription_obj.created_at = new Date();
        subscription_obj.last_renewal_at = new Date();
        subscription_obj.frequency = frequency;
        subscription_obj.status = 'Active';
        var valid_upto = new Date();

        if(planData.balances && planData.balances.length > 0 && planData.provideBalance){
          subscription_obj.balances = [];
          var planBalances = planData.balances;
          subscription_obj.balances = payment.insertBalanceToSubscription(subscription_obj.balances, planBalances, plan_type)
        }
  
          //checking the frequency of the plan
        if (frequency == 'MONTH') {
            subscription_obj.valid_upto = new Date(new Date(valid_upto).setMonth(valid_upto.getMonth() + 1));
        } else {
            subscription_obj.valid_upto = new Date(new Date(valid_upto).setMonth(valid_upto.getMonth() + 12));
        }

        subscription_obj.valid_upto = setEndTimeToDate(subscription_obj.valid_upto);

        subscription_obj.product = product;
        subscription_obj.plan_type = plan_type;
  
        if (transaction_obj.comments) {
          subscription_obj.comments = transaction_obj.comments;
        }
        if (transaction_obj.payer_email) {
            subscription_obj.payer_email = transaction_obj.payer_email
        }

        result = await databaseManager.insertToKind("subscription", subscription_obj)
        console.log("result in save subscription-->", result)
        await payment.updateProductUser(subscription_obj.email, subscription_obj.product)
        await payment.addProductInContact(subscription_obj);
        result.existingPlan = "Basic";
    }
    return result;
}



async function insertToTransactionDowngrade(transaction_obj, frequency, product, plan_code, paymentGatewayType, existingSub) {

    // Inserting the payment info to the transaction collection
    transaction = {};
    transaction.email = transaction_obj.license_email;
    transaction.plan_type = plan_code;
    transaction.product = product;
    transaction.frequency = frequency;
    transaction.created_at = new Date();
  
    transaction.payment_gateway_type = paymentGatewayType;
  
    // checking for the field payment_date in paypal_transaction 
    //if the field is there in that kind we need to take that payment_date else taking the new date 
  
    if (transaction_obj.payment_date) {
        transaction_obj.payment_date = new Date(transaction_obj.payment_date);
        transaction.transaction_date = new Date(transaction_obj.payment_date);
    } else {
      transaction.transaction_date = new Date();
    }
  
    transaction.type = "DOWNGRADE";
  
    // if (transaction_obj.txn_type) {
    //   transaction.transaction_type = transaction_obj.txn_type;
    // }
  
    if (transaction_obj.status) {
      transaction.payment_status = transaction_obj.status;
    }
  
    transaction.valid_from = new Date();
    var valid_upto = new Date(existingSub.valid_upto) || '';
    transaction.valid_upto = valid_upto;
    // if (frequency == 'MONTH') {
    //   transaction.valid_upto = new Date(new Date(valid_upto).setMonth(valid_upto.getMonth() + 1));
    // } else if (frequency == 'YEAR') {
    //   transaction.valid_upto = new Date(new Date(valid_upto).setMonth(valid_upto.getMonth() + 12));
    // } else {
    //   transaction.valid_upto = '';
    // }
  
    if (transaction_obj.payer_email) {
      transaction.payer_email = transaction_obj.payer_email;
    }
  
    if (transaction_obj.payment_date) {
      transaction.payment_date = transaction_obj.payment_date;
    }
    // if (transaction_obj.payment_fee) {
    //   transaction.payment_fee = transaction_obj.payment_fee
    // }
    // if (transaction_obj.billing_info) {
    //   transaction.payment_gross = transaction_obj.payment_gross
    // }
    if (transaction_obj.mc_currency) {
      transaction.currency = transaction_obj.mc_currency;
    }
    transaction.payment_net = Number(transaction_obj.payment_gross)//Number(transaction_obj.payment_gross) - Number(transaction_obj.payment_fee);
  
    var storedtransaction = await databaseManager.insertToKind("transaction", transaction);
    console.log("transaction --->", transaction)
    return storedtransaction;
  }

  /*
update Subscription For Upgrade
*/
async function updateSubscriptionForUpgrade(transaction_obj, reviseType, planData){
  console.log("record is going to update for subscription collection");
  
  var plan_type = planData.plan_type; // Pro, Premium, Business etc
  var product = planData.product;
  var frequency = planData.frequency;
  
  const query_for_subscription = datastoreClient.createQuery("subscription")
    .filter("email", "=", transaction_obj.license_email)
    .filter("product", "=", product);
  var result = null;
  let existingSub = await databaseManager.getKindData(query_for_subscription);

  if(existingSub) {
      console.log("existing subscription", existingSub)
      existingSub.status = 'Active';
      existingSub.subscription_id = transaction_obj.subscription_id;
      existingSub.frequency = (frequency) ? frequency : existingSub.frequency; // TODO - check if defaulting with entity.frequency required
      existingSub.email = transaction_obj.license_email;
      existingSub.last_renewal_at = new Date();
      if (transaction_obj.payer_email) {
          existingSub.payer_email = transaction_obj.payer_email
      }
      var existingPlan = existingSub.plan_type;
      existingSub.plan_type = plan_type;
      if(existingSub.revision && existingSub.revision.licence_type && existingSub.revision.licence_type == "Domain"){
        var adminResult = null;
        existingSub.revision = null;
        adminResult = await databaseManager.updateKind(existingSub);
        adminResult.existingPlan = existingPlan;
        //update for other users in group
        if(existingSub.group_code){
          result = await updateSubscriptionForGroup(existingSub.group_code, planData, transaction_obj, existingSub.email)
          result.push(adminResult);
        } else {
          await payment.updateProductUser(existingSub.email, existingSub.product);
          result = adminResult;
        }
      } else {
        existingSub.revision = null;
        result = await databaseManager.updateKind(existingSub);
        result.existingPlan = existingPlan;
        await payment.updateProductUser(existingSub.email, existingSub.product);
      }
  }
  console.log("result in update SubscriptionForUpgrade-->", result) 
  return result;
}
  

/*
update/renew subscription For Group emails
@param group_code group code
@param planData plan map
@param transaction_obj paypal subscription obj
*/
async function updateSubscriptionForGroup(group_code, planData, transaction_obj, licenseEmail){
  console.log("update subscription for group code:", group_code)
  var result = [];
  var plan_type = planData.plan_type; // Pro, Premium, Business etc
  var plan_code = planData.code; // item number from the transaction
  var product = planData.product;
  var frequency = planData.frequency;
  var limitedUsers = (planData.users == 'UNLIMITED') ? planData.users : Number(planData.users);
  var query = datastoreClient.createQuery('subscription').filter("group_code", '=', group_code)
  var option = {size: 200};
  var response = await databaseManager.getAllDataFromKind(query, option);
  var groupUsers = response[0];
  if(groupUsers.length > 0){
    for(var i = 0; i < groupUsers.length; i++){
      var obj = {};
      var email = groupUsers[i].email || null;
      if(email && email != licenseEmail){
        const query_for_subscription = datastoreClient.createQuery("subscription")
        .filter("email", "=", email)
        .filter("product", "=", product);
        let existingSub = await databaseManager.getKindData(query_for_subscription);
        existingSub.status = 'Active';
        existingSub.subscription_id = transaction_obj.subscription_id;
        existingSub.frequency = (frequency) ? frequency : existingSub.frequency; // TODO - check if defaulting with entity.frequency required
        existingSub.email = email;
        existingSub.last_renewal_at = new Date();
        if (transaction_obj.payer_email) {
          existingSub.payer_email = transaction_obj.payer_email;
        }
        var existingPlan = existingSub.plan_type;
        existingSub.plan_type = plan_type;
        existingSub.revision = null;
        obj = await databaseManager.updateKind(existingSub);
        obj.existingPlan = existingPlan;
        await payment.updateProductUser(existingSub.email, existingSub.product);
        result.push(obj);
        if((limitedUsers != 'UNLIMITED') && (i == limitedUsers - 1)) break; //break if limited users exists
      }
    }
  }
  return result;
}

/*
 * updateSubscriptionStatusForSubscriptionCancel is a function to update the status of canceled users.
 * @param {*} email , email id of the subscribed user
 * @param {*} product , product of the subscribed user 
 * @param {*} callback , callback function
 */
async function updateSubscriptionStatusForSubscriptionCancel(email, product) {
  console.log("calling updateSubscriptionStatusForSubscriptionCancel function in subscription kind")
  const query_for_subscription = datastoreClient.createQuery("subscription")
    .filter("email", "=", email)
    .filter("product", "=", product);
  let existingSub = await databaseManager.getKindData(query_for_subscription);

  if(existingSub){
      existingSub.status = "Cancelled";
      await databaseManager.updateKind(existingSub);
  } else {
      console.log("No record found to update the cancelled status")
  }
}


/*
  manage paypal subscription activation on renewal
*/
async function managePaypalSubscriptionActivationOnRenewal(subscriberObj, licenseEmail, planId, groupCode){

  // subscriberObj.license_email = licenseEmail.toLowerCase();
  // subscriberObj.subscription_id = subscriberObj.id;
  // subscriberObj.payer_email = subscriberObj.subscriber.email_address.toLowerCase();
  // if(subscriberObj.links) delete subscriberObj.links; //delete links array
  // subscriberObj = createTransactionObj(subscriberObj);

  //check existing TODO--------

  // //put in paypal events
  // var storedPaypalEvent = await databaseManager.insertToKind("paypal_event", subscriberObj);

  var planData = await getPlanFromSMS(planId);
  var plan_type = planData.plan_type; // Pro, Premium, Business etc
  var plan_code = planData.code; // item number from the transaction
  var product = planData.product;
  var frequency = planData.frequency;
  var licence_type = planData.licence_type;
  var paypal_plan_id = planData.paypal_plan_id;
  renewSubscription(subscriberObj, frequency, product, plan_type, planData, groupCode, subscriberObj.license_email)
}

/*
update Subscription For Upgrade on renewal
@param transaction_obj subscription obj
@param frequency frequency
@param product product
@param plan_type plan_type
*/
async function renewSubscription(transaction_obj, frequency, product, plan_type, planData, groupCode, licenseEmail){
  console.log("record is going to update for subscription collection");
  if(planData.licence_type && planData.licence_type == "Domain" && groupCode){
    //first subscription for update admin email
    await payment.insertAndUpdateSubscription(transaction_obj, planData)

    var limitedUsers = (planData.users == 'UNLIMITED') ? planData.users : Number(planData.users);
    var query = datastoreClient.createQuery('subscription').filter("group_code", '=', group_code)
                .filter("product", "=", product).limit(200);
    var response = await databaseManager.getAllDataFromKind(query);
    var groupUsers = response ? response[0] : [];
    if(groupUsers.length > 0){
      for(var i = 0; i < groupUsers.length; i++){
        var email = groupUsers[i].email || null;
        transaction_obj.license_email = email;
        if(email && email != licenseEmail){
          await payment.insertAndUpdateSubscription(transaction_obj, planData)
          if((limitedUsers != 'UNLIMITED') && (i == limitedUsers - 1)) break; //break if limited users exists
        }
      }
    }   
  } else {
    await payment.insertAndUpdateSubscription(transaction_obj, planData)
  }
  // return result;
}


/*
 store cancel event in paypal_event and insert into transaction
 @param subscriberObj subscriberObj
 @param licenseEmail 
 @param planId paypal planId
*/
async function manageCancelEvent(subscriberObj, licenseEmail, planId){
  // subscriberObj.license_email = licenseEmail.toLowerCase();
  // subscriberObj.subscription_id = subscriberObj.id;
  // subscriberObj.payer_email = subscriberObj.subscriber.email_address.toLowerCase();
  // if(subscriberObj.links) delete subscriberObj.links; //delete links array
  // subscriberObj = createTransactionObj(subscriberObj);

  var planData = await getPlanFromSMS(planId);
  var plan_code = planData.code; // item number from the transaction
  var product = planData.product;
  var frequency = planData.frequency
  await payment.insertToTransaction(subscriberObj, frequency, product, plan_code, 'PAYPAL-SMART');
}

  
  // var subscriptionMap = body.resource;
  // var subscriptionId = subscriptionMap.id;

  // //put in paypal events
  // subscriptionMap.subscription_id = subscriptionMap.id;
  // subscriptionMap.payer_email = subscriptionMap.subscriber.email_address.toLowerCase();
  // if(subscriptionMap.links) delete subscriptionMap.links; //delete links array
  // subscriptionMap = createTransactionObj(subscriptionMap);
  // await databaseManager.insertToKind("paypal_event", subscriptionMap);

  // var subscriptionSMSMap = await getSubscriptionDetailsFromSms(subscriptionId);
  // if(subscriptionSMSMap){
  //   var licenseEmail = subscriptionSMSMap.email;
  //   var planId = subscriptionSMSMap.plan_id;
  //   subscriptionMap.license_email = licenseEmail.toLowerCase();

  //   if(event == "BILLING.SUBSCRIPTION.RENEWED"){
  //     var groupCode = subscriptionSMSMap.group_code ? subscriptionSMSMap.group_code : null;
  //     await managePaypalSubscriptionActivationOnRenewal(subscriptionMap, licenseEmail, planId, groupCode);
  //   } else if(event == "BILLING.SUBSCRIPTION.CANCELLED"){
  //     await manageCancelEvent(subscriptionMap, licenseEmail, planId);
  //   }
  // }

      //  await axios(url, {
    //       method : 'POST',
    //       headers: {
    //           "Accept": "application/json",
    //           "Accept-Language": "en_US",
    //           "content-type": "application/x-www-form-urlencoded"
    //       },
    //       params: {
    //           grant_type: 'client_credentials',
    //       },
    //       auth: {
    //           'username': client,
    //           'password': secret,
    //           // 'sendImmediately': false
    //       },
    //   }).then((res) =>{
    //       console.log("result in getting paypal access token", res.data.access_token)
    //       result = res.data.access_token;
    //   }).catch((error) => {
    //       console.log("error", error)
    //       throw error;
    //   })